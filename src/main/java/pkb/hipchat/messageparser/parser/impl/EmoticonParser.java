package pkb.hipchat.messageparser.parser.impl;

import org.apache.commons.lang.StringUtils;
import pkb.hipchat.messageparser.parser.api.ParserIntf;

import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Implementation of ParserIntf interface to parse emoticons.
 *
 * @author pkb
 */
public class EmoticonParser implements ParserIntf<Optional<String>> {
    private static final Logger LOG = Logger.getLogger(EmoticonParser.class.getName());

    private static final int MIN_LENGTH = 1;
    private static final int MAX_LENGTH = 15;

    /**
     * Parses input string and returns optional string containing the emoticon
     * text inside the parenthesis as output. Validates that the input has
     * matching parentheses and the emoticon length is not over 15 chars
     *
     * Emoticon definition - We only consider 'custom' emoticons which are
     * alphanumeric strings, no longer than 15 characters, contained in
     * parenthesis. Anything matching this format is an emoticon.
     *
     * @param input String with emoticon
     * @return Optional String emoticon value
     */
    @Override
    public Optional<String> parse(String input) {
        LOG.info("Parsing input for emoticon: " + input);
        final String result = input.substring(1, input.length()-1);
        if (result.length() >= MIN_LENGTH && result.length() <= MAX_LENGTH && StringUtils.isAlphanumeric(result)) {
            LOG.info("Parsed emoticon: " + result);
            return Optional.of(result);
        }
        return Optional.empty();
    }
}
