package pkb.hipchat.messageparser.parser;

import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import pkb.hipchat.messageparser.parser.impl.EmoticonParser;

import java.util.Optional;

/**
 * Created by pkb on 12/11/16.
 */
public class EmoticonParserTest {

    private EmoticonParser parser;

    @Before
    public void setUp() throws Exception {
        parser = new EmoticonParser();
    }

    @Test
    public void mustParseValidEmoticon() {
        Optional<String> actual = parser.parse("(emoticon123)");
        Assert.assertEquals(Optional.of("emoticon123"), actual);
    }

    @Test
    public void mustReturnEmptyForNonAlphaNumericInput() {
        Optional<String> actual = parser.parse("(dummy*emoticon)");
        Assert.assertEquals(Optional.empty(), actual);
    }

    @Test
    public void mustReturnEmptyForZeroLengthEmoticon() {
        Optional<String> actual = parser.parse("()");
        Assert.assertEquals(Optional.empty(), actual);
    }

    @Test
    public void mustReturnEmptyIfGreaterThanMaxCharLimit() {
        Optional<String> actual = parser.parse("(more_than_fifteen_characters)");
        Assert.assertEquals(Optional.empty(), actual);
    }
}
