package pkb.hipchat.messageparser.parser;

import junit.framework.Assert;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import pkb.hipchat.messageparser.parser.impl.MessageParser;
import pkb.hipchat.messageparser.types.Link;
import pkb.hipchat.messageparser.types.ParsedResult;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;

/**
 * Created by pkb on 12/11/16.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({Jsoup.class})
public class MessageParserTest {

    private MessageParser parser;
    private Connection connection;

    @Before
    public void setUp() throws Exception {
        parser = new MessageParser();

        connection = Mockito.mock(Connection.class);
        PowerMockito.mockStatic(Jsoup.class);
        PowerMockito.when(Jsoup.connect(Mockito.anyString())).thenReturn(connection);
    }

    @Test
    public void mustParseAllValidItemsMessage() throws Exception {
        Document document = Mockito.mock(Document.class);
        Mockito.when(document.title()).thenReturn("http://dummylink.com");
        Mockito.when(document.title()).thenReturn("Dummy title");
        Mockito.when(connection.get()).thenReturn(document);

        ParsedResult actual = parser.parse("@mention1 (emoticon1) http://dummylink.com abcdef").get();

        ParsedResult expected = new ParsedResult();
        expected.setMentions(new HashSet<>(Arrays.asList("mention1")));
        expected.setEmoticons(new HashSet<>(Arrays.asList("emoticon1")));
        expected.setLinks(new HashSet<>(Arrays.asList(new Link("http://dummylink.com", "Dummy title"))));
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void mustIgnoreUnparsedItems() throws Exception {
        Mockito.when(connection.get()).thenThrow(new IOException());
        ParsedResult actual = parser.parse("@mention1 (emoticon1) http://invalidlink abcdef").get();

        ParsedResult expected = new ParsedResult();
        expected.setMentions(new HashSet<>(Arrays.asList("mention1")));
        expected.setEmoticons(new HashSet<>(Arrays.asList("emoticon1")));
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void mustReturnEmptyResultWhenNoParsableItemsFound() throws Exception {
        ParsedResult actual = parser.parse("abcdef").get();

        ParsedResult expected = new ParsedResult();
        Assert.assertEquals(expected, actual);
    }
}
