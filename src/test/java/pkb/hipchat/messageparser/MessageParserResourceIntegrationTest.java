package pkb.hipchat.messageparser;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.grizzly.http.server.HttpServer;

import org.glassfish.jersey.jackson.JacksonFeature;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import pkb.hipchat.messageparser.types.Link;
import pkb.hipchat.messageparser.types.ParsedResult;

import java.util.Arrays;
import java.util.HashSet;

import static org.junit.Assert.assertEquals;

public class MessageParserResourceIntegrationTest {

    private HttpServer server;
    private WebTarget target;

    @Before
    public void setUp() throws Exception {
        // start the server
        server = Main.startServer();
        // create the client
        Client c = ClientBuilder.newBuilder().register(JacksonFeature.class).build();
        target = c.target(Main.BASE_URI);
    }

    @After
    public void tearDown() throws Exception {
        server.stop();
    }

    /**
     * Test to see that the message "I'm up!" is sent in the response.
     */
    @Test
    public void testPing() {
        String responseMsg = target.path("parser/ping").request().get(String.class);
        assertEquals("I'm up!", responseMsg);
    }

    @Test
    public void testParse() {
        Response responseMsg = target.path("parser/parse").request().post(Entity.entity(
                "@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016",
                MediaType.TEXT_PLAIN
        ));
        ParsedResult actual = responseMsg.readEntity(ParsedResult.class);

        ParsedResult expected = new ParsedResult();
        expected.setMentions(new HashSet<>(Arrays.asList("bob", "john")));
        expected.setEmoticons(new HashSet<>(Arrays.asList("success")));
        expected.setLinks(new HashSet<>(Arrays.asList(
                new Link(
                        "https://twitter.com/jdorfman/status/430511497475670016",
                        "Justin Dorfman on Twitter: &quot;nice @littlebigdetail from @HipChat (shows hex colors when pasted in chat). http://t.co/7cI6Gjy5pq&quot;"))));

        assertEquals(200, responseMsg.getStatus());
        assertEquals(expected, actual);
    }
}
