package pkb.hipchat.messageparser.parser.api;

/**
 * Interface defining parse function that needs to be implemented for each type
 * that we want to parse.
 */
public interface ParserIntf<Optional> {
    /**
     * Method handling the parse logic for a given type. Takes in a string
     * input and returns an Optional parsed value. If the input is not valid or
     * there is an exception while parsing, an empty Optional is returned.
     *
     * @param input String input that needs to be parsed
     * @return Parsed value wrapped as an Optional
     */
    Optional parse(String input);
}
