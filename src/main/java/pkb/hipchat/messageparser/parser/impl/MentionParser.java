package pkb.hipchat.messageparser.parser.impl;

import com.google.common.base.Strings;
import pkb.hipchat.messageparser.parser.api.ParserIntf;

import java.util.Optional;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Implementation of ParserIntf interface to parse mentions.
 *
 * @author pkb
 */
public class MentionParser implements ParserIntf<Optional<String>> {
    private static final Logger LOG = Logger.getLogger(MentionParser.class.getName());
    private static final String REGEX = "@\\w+";
    private static final Pattern pattern = Pattern.compile(REGEX);

    /**
     * Parses input string with '@' prefix and returns optional string
     * containing the mention text. Returns only the alphanumeric part after
     * '@' symbol
     *
     * Mentions definition - Mention is a way to mention a user. Always starts
     * with an '@' and ends when hitting a non-word character.
     *
     * @param input String with mention containing '@' prefix
     * @return Optional String mention value
     */
    @Override
    public Optional<String> parse(String input) {
        LOG.info("Parsing input for mention: " + input);
        final Matcher matcher = pattern.matcher(input);
        if (matcher.find()) {
            final String match = matcher.group().substring(1);
            if (!Strings.isNullOrEmpty(match)) {
                LOG.info("Parsed mention: " + match);
                return Optional.of(match);
            }
        }
        return Optional.empty();
    }
}
