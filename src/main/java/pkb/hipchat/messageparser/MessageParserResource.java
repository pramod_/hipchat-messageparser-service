package pkb.hipchat.messageparser;

import pkb.hipchat.messageparser.parser.impl.MessageParser;
import pkb.hipchat.messageparser.types.ParsedResult;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Optional;

/**
 * Root resource (exposed at "hipchatmessaging" path)
 */
@Path("parser")
@Produces(MediaType.APPLICATION_JSON)
public class MessageParserResource {

    private final MessageParser messageParser;

    public MessageParserResource() {
        this.messageParser = new MessageParser();
    }

    /**
     * Method handling HTTP GET requests. The returned object will be sent
     * to the client as "text/plain" media type.
     *
     * @return String that will be returned as a text/plain response.
     */
    @GET
    @Path("ping")
    @Produces(MediaType.TEXT_PLAIN)
    public String ping() {
        return "I'm up!";
    }

    /**
     * Method handling HTTP POST requests to parse message in "text/plain"
     * format. Returns ParsedResult as JSON output to the client with parsed
     * values of mentions, emoticons and links from the input.
     *
     * @param message Input message string
     * @return ParsedResult as application/json type response
     */
    @POST
    @Path("parse")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    public ParsedResult parse(String message) {
        final Optional<ParsedResult> result = messageParser.parse(message);
        return result.orElse(new ParsedResult());
    }
}
