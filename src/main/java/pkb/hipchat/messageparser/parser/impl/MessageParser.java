package pkb.hipchat.messageparser.parser.impl;

import pkb.hipchat.messageparser.parser.api.ParserIntf;
import pkb.hipchat.messageparser.types.Link;
import pkb.hipchat.messageparser.types.ParsedResult;

import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Implementation of ParserIntf as a composite parser which parses mentions,
 * emoticons and links.
 *
 * @author pkb
 */
public class MessageParser implements ParserIntf<Optional<ParsedResult>> {
    private static final Logger LOG = Logger.getLogger(MessageParser.class.getName());

    private static final String REGEX = "\\S+";
    private static final Pattern pattern = Pattern.compile(REGEX);

    private static final String MENTION_TOKEN_TYPE = "mention";
    private static final String EMOTICON_TOKEN_TYPE = "emoticon";
    private static final String LINK_TOKEN_TYPE = "link";

    private static final ExecutorService executor = Executors.newWorkStealingPool();

    private final Map<String, ParserIntf> parsers = new HashMap<>();

    public MessageParser() {
        registerParsers();
    }

    private void registerParsers() {
        parsers.put(MENTION_TOKEN_TYPE, new MentionParser());
        parsers.put(EMOTICON_TOKEN_TYPE, new EmoticonParser());
        parsers.put(LINK_TOKEN_TYPE, new LinkParser());
    }

    /**
     * Parses input string for mentions, emoticons and links by delegating work
     * to different parsers based on the matching characters.
     *
     * @param message String input containing the message text
     * @return Optional ParsedResult with parsed values
     */
    @Override
    public Optional<ParsedResult> parse(String message) {
        LOG.info("Parsing message: " + message);
        final long tic = System.currentTimeMillis();

        // Assuming return order doesn't matter, otherwise will need to maintain
        // ordering (using LinkedHashSet ordered by position in input for example)
        final Set<String> mentions = Collections.synchronizedSet(new HashSet<>());
        final Set<String> emoticons = Collections.synchronizedSet(new HashSet<>());
        final Set<Link> links = Collections.synchronizedSet(new HashSet<>());

        final List<Callable<Optional>> callables = generateTasks(message, mentions, emoticons, links);

        executeTasks(callables);

        final ParsedResult result = new ParsedResult();
        if (!mentions.isEmpty()) {
            result.setMentions(mentions);
        }
        if (!emoticons.isEmpty()) {
            result.setEmoticons(emoticons);
        }
        if (!links.isEmpty()) {
            result.setLinks(links);
        }
        final long toc = System.currentTimeMillis();
        LOG.info("Parsed result: " + result + " in " + (toc - tic) + "ms");
        return Optional.of(result);
    }

    private List<Callable<Optional>> generateTasks(
            final String message,
            final Set<String> mentions,
            final Set<String> emoticons,
            final Set<Link> links) {
        final List<Callable<Optional>> callables = new ArrayList<>();

        final Matcher matcher = pattern.matcher(message);
        while (matcher.find()) {
            final String token = matcher.group();
            if (token.startsWith("@")) {
                callables.add(generateCallable(MENTION_TOKEN_TYPE, token, mentions));
            }
            else if (token.startsWith("(") && token.endsWith(")")) {
                callables.add(generateCallable(EMOTICON_TOKEN_TYPE, token, emoticons));
            }
            else if (token.toLowerCase().startsWith("http://") || token.toLowerCase().startsWith("https://")) {
                callables.add(generateCallable(LINK_TOKEN_TYPE, token, links));
            }
        }
        return callables;
    }

    private void executeTasks(final List<Callable<Optional>> callables) {
        try {
            executor.invokeAll(callables)
                    .stream()
                    .map(future -> {
                        try {
                            return future.get();
                        }
                        catch (Exception e) {
                            throw new IllegalStateException(e);
                        }
                    });
        } catch (InterruptedException e) {
            LOG.log(Level.WARNING, "Exception while parsing input:" + e);
        }
    }

    private Callable<Optional> generateCallable(
            final String tokenType,
            final String inputToken,
            final Set outputCollection) {
        return () -> {
            final Optional parsedValue = (Optional) parsers.get(tokenType).parse(inputToken);
            parsedValue.ifPresent(outputCollection::add);
            return null;
        };
    }
}
