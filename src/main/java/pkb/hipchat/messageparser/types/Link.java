package pkb.hipchat.messageparser.types;

import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.Objects;

/**
 * Data structure to represent Link in a message. Contains a string URL and the
 * string title corresponding to the URL.
 */
public class Link {
    private String url;
    private String title;

    public Link() {
    }

    public Link(String url, String title) {
        this.url = url;
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Link link = (Link) o;
        return Objects.equals(url, link.url) &&
                Objects.equals(title, link.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(url, title);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("url", url)
                .append("title", title)
                .toString();
    }
}
