package pkb.hipchat.messageparser.parser;

import junit.framework.Assert;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import pkb.hipchat.messageparser.parser.impl.LinkParser;
import pkb.hipchat.messageparser.types.Link;

import java.io.IOException;
import java.util.Optional;

/**
 * Created by pkb on 12/11/16.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({Jsoup.class})
public class LinkParserTest {

    private LinkParser parser;
    private Connection connection;

    @Before
    public void setUp() throws Exception {
        parser = new LinkParser();

        connection = Mockito.mock(Connection.class);
        PowerMockito.mockStatic(Jsoup.class);
        PowerMockito.when(Jsoup.connect(Mockito.anyString())).thenReturn(connection);
    }

    @Test
    public void mustParseValidLink() throws Exception {
        Document document = Mockito.mock(Document.class);
        Mockito.when(document.title()).thenReturn("http://dummylink.com");
        Mockito.when(document.title()).thenReturn("Dummy title");
        Mockito.when(connection.get()).thenReturn(document);

        Optional<Link> actual = parser.parse("http://dummylink.com");
        Assert.assertEquals(Optional.of(new Link("http://dummylink.com", "Dummy title")), actual);
    }

    @Test
    public void mustReturnUrlWhenTitleUnavailable() throws Exception {
        Document document = Mockito.mock(Document.class);
        Mockito.when(document.title()).thenReturn("http://dummylink.com");
        Mockito.when(document.title()).thenReturn("");
        Mockito.when(connection.get()).thenReturn(document);

        Optional<Link> actual = parser.parse("http://dummylink.com");
        Assert.assertEquals("http://dummylink.com", actual.get().getTitle());
    }

    @Test
    public void mustIgnoreInvalidLink() throws Exception {
        Mockito.when(connection.get()).thenThrow(new IOException());

        Optional<Link> actual = parser.parse("http://invalidlink");
        Assert.assertEquals(Optional.empty(), actual);
    }
}
