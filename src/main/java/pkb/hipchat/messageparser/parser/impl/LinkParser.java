package pkb.hipchat.messageparser.parser.impl;

import com.google.common.base.Strings;
import org.apache.commons.lang.StringEscapeUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import pkb.hipchat.messageparser.parser.api.ParserIntf;
import pkb.hipchat.messageparser.types.Link;

import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Implementation of ParserIntf interface to parse links.
 *
 * @author pkb
 */
public class LinkParser implements ParserIntf<Optional<Link>> {
    private static final Logger LOG = Logger.getLogger(LinkParser.class.getName());

    /**
     * Parses input string and returns optional Link containing the url and
     * title. Validates the input url is not malformed. If no title is found,
     * the url value is used for the title as well. Empty Optional is returned
     * if any exceptions occur during the parsing.
     *
     * Links definition - Any URLs contained in the message, along with the
     * page's title.
     *
     * @param input String url
     * @return Optional Link with page URL and Title
     */
    @Override
    public Optional<Link> parse(String input) {
        try {
            LOG.info("Parsing link: " + input);
            final Document document = Jsoup.connect(input).get();

            final String title = StringEscapeUtils.escapeHtml(document.title());
            final Link result = new Link(input, Strings.isNullOrEmpty(title) ? input : title);
            LOG.info("Parsed link: " + result);
            return Optional.of(result);
        } catch (Exception e) {
            LOG.log(Level.WARNING, "Exception while parsing link", e);
        }
        return Optional.empty();
    }
}
