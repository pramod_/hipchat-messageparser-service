# hipchat-messageparser-service

**RESTful service to parse information in a chat message**

### What it does ###
RESTful API that takes a chat message string as input and parses the contents for information about the following types:

* **Mentions** - A way to mention a user. Always starts with an '@' and ends when hitting a non-word character.
* **Emoticons** - 'custom' emoticons which are alphanumeric strings, no longer than 15 characters, contained in parenthesis.
* **Links** - Any URLs contained in the message, along with the page's title.

The information is returned in the form of a JSON response object containing arrays of all matches parsed from the input string.
(See the *Run* section for sample request and response)

### Build ###
Run `mvn clean compile test` to build the project and run the tests (requires Java 1.8 or above).


### Run ###
Run the server from main method in `pkb.hipchat.messageparser.Main` from IDE or run using the maven exec plugin as follows
`mvn clean compile exec:java`.

After starting the server, we can check that everything works by executing a curl command.
```bash
  echo '@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016' | curl -X POST -HContent-Type:text/plain http://localhost:8080/hipchatmessaging/parser/parse  --data @-
```

This should produce a JSON response like below.
```json
  {"mentions":["bob","john"],"emoticons":["success"],"links":[{"url":"https://twitter.com/jdorfman/status/430511497475670016","title":"Justin Dorfman on Twitter: &quot;nice @littlebigdetail from @HipChat (shows hex colors when pasted in chat). http://t.co/7cI6Gjy5pq&quot;"}]}
```
There is also an integration test in the code (`pkb.hipchat.messageparser.MessageParserResourceIntegrationTest`) that executes a similar test using Java REST client.

### API ###
Base URI is `http://localhost:8080/hipchatmessaging`.
Following resources are relative to the base URI.

Resource        | HTTP Method | Description 
--------------  | :-----:     |:--------------
`/parser/ping`  |  GET        | Simple API that returns _I'm up_. Can be used to check that the server is running.
`/parser/parse` |  POST       | Accepts a plain text input and returns JSON reponse containing mentions, emoticons and links parsed from the input message.

### Notes ###
* Built based on `jersey-quickstart-grizzly2` maven archetype. Uses Jersey framework and Grizzly HTTP server.
* Parsers for each type that we care about are implemented against a common interface. These parsers are composed to yield the final message parser which itself is implemented against the common parser interface.
* A task is fired off for each matching token and Executors are used to run them. Parsing links involves network I/O which is slow, and this will help to run them in parallel. This will be even more useful if we decide to add another parser that involves I/O like querying a database to get some information.
* The response does not contain duplicates. This is done by using a `HashSet` to store the parsed values. `List` can be used if all instances need to be returned.
* Using `HashSet` also means that order is not preserved. If ordering needs to be maintained, we can use `LinkedHashSet` ordered by position in input string.
* To keep the code simple, there is no dependency injection framework used. As the code grows in complexity, frameworks like Google Guice or Spring can be used for dependency injection. That will help in testing too.
* Jackson library is used to automatically convert the POJO result to JSON object while sending the response.