package pkb.hipchat.messageparser.types;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.Objects;
import java.util.Set;

/**
 * Data structure to hold the parsed result containing the collections of
 * mentions, emoticons and links.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ParsedResult {
    private Set<String> mentions;
    private Set<String> emoticons;
    private Set<Link> links;

    public Set<String> getMentions() {
        return mentions;
    }

    public void setMentions(Set<String> mentions) {
        this.mentions = mentions;
    }

    public Set<String> getEmoticons() {
        return emoticons;
    }

    public void setEmoticons(Set<String> emoticons) {
        this.emoticons = emoticons;
    }

    public Set<Link> getLinks() {
        return links;
    }

    public void setLinks(Set<Link> links) {
        this.links = links;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final ParsedResult result = (ParsedResult) o;
        return Objects.equals(mentions, result.mentions) &&
                Objects.equals(emoticons, result.emoticons) &&
                Objects.equals(links, result.links);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mentions, emoticons, links);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("mentions", mentions)
                .append("emoticons", emoticons)
                .append("links", links)
                .toString();
    }
}
