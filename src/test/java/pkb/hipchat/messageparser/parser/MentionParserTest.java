package pkb.hipchat.messageparser.parser;

import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import pkb.hipchat.messageparser.parser.impl.MentionParser;

import java.util.Optional;

/**
 * Created by pkb on 12/11/16.
 */
public class MentionParserTest {

    private MentionParser parser;

    @Before
    public void setUp() throws Exception {
        parser = new MentionParser();
    }

    @Test
    public void mustParseValidMention() {
        Optional<String> actual = parser.parse("@dummy_mention");
        Assert.assertEquals(Optional.of("dummy_mention"), actual);
    }

    @Test
    public void mustParseWordCharactersOnly() {
        Optional<String> actual = parser.parse("@dummy*mention");
        Assert.assertEquals(Optional.of("dummy"), actual);
    }

    @Test
    public void mustReturnEmptyForInvalidMention() {
        Optional<String> actual = parser.parse("invalidmention");
        Assert.assertEquals(Optional.empty(), actual);
    }
}
